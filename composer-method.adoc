== Introduction to Lorax Composer

=== Architecture

image:labImages/Composer-arch.jpg[]

=== Disable SELinux to workaround a bug

This lab is using a pre-GA version of RHEL8, which includes a bug that prevents Composer working correctly when SELinux is enabled. Therefore, we will disable SELinux for the rest of the lab.

Verify that SELinux is currently turned on (this is the default);

    root@workstation-GUID: getenforce
    Enforcing

If you are not familiar with SELinux, it is an extra layer of security on RHEL,
providing mandatory access control over many common system services. It also
reinforces many of the security controls around virtual machines on RHEL and
containers on RHEL.

The bug has been fixed in the General Availability version of RHEL8 (released
at Summit), and this step will no longer be necessary outside of this lab. 

The virtual machine gold images that you build will have SELinux turned on, and that works fine.

To turn off SELinux, edit the file: `/etc/sysconfig/selinux` to match the following;

    SELINUX=permissive

[NOTE]
Permissive mode turns off SELinux's enforcing mode, not providing the same
level of security, but it will log security issues. You can also set it to
 "disabled" to completely disable SELinux.

While it is not always necessary to reboot when SELinux is disabled, it's good practice and a safe precaution. You should **reboot** the lab `workstation-GUID` machine now using the `reboot` command.

Once the lab machine boots up again (takes 2-3 minutes), connect via SSH and Cockpit again. 

Once reconnected, run `sudo su -` again to become root, and check that SELinux is now in Permissive mode;

    root@workstation-GUID: getenforce
    Permissive

=== Install composer, and the web GUI plugin

Run the following commands below to install composer and it's dependencies.

    root@workstation-GUID: yum install lorax lorax-composer composer-cli cockpit-composer

This should prompt to install about 90 extra packages. Wait for the
installation to complete.

We'll now start the composer service. The composer service contains a set of tools and daemons that run, and actually build images. Tools like the cockpit Web UI and `composer-cli` talk back to this service.

    root@workstation-GUID: systemctl enable --now lorax-composer.socket

Finally, to get the cockpit web interface to load the new plugin for Cockpit, we need to start the service.

    root@workstation-GUID: systemctl restart cockpit.service

You will need to login again to the cockpit web interface because it has been restarted.

Now lets navigate to the cockpit web interface again, and see the Composer
(Image Builder) plugin appear in the sidebar; 

image:labImages/composerGui.png[]

This is the web interface on top of composer that makes it easy to create simple images. The `composer-cli` has more features, but this is good to be getting started with.

=== Introduction to blueprints 

Blueprints define the paramaters for composer to build an image, they're the "instructions" that make up images. 

    +--------------+         +----------+
    | Blueprints   |-------->| Images   |
    +--------------+         +----------+
      Instructions:            Azure VHD file
      - include bash           (vm gold image)
      - setup default user
      - etc... 


You will see that there are 4 blueprints provided for you - `example-atlas`,
`example-development`, `example-http-server` and `goldimage`. You can explore these blueprints to get a better understanding of what they look like. 

==== Create a blueprint

Let's create a new blueprint, using the button in the top right corner of the screen; 

image:labImages/createBlueprintButton.png[]

This will present you with a create blueprint dialog, give your image a descriptive name; 

image:labImages/createBlueprintDialog.png[]

When you edit the blueprint, you'll get a screen that looks like this;

image:labImages/blueprintAddComponents.png[]

== Create a simple image from the GUI

Obviously a basic Linux image needs some key packages, like systemd, a kernel, a bash shell and so forth. The lorax composer service will always make sure dependencies and critical packages are included. 

=== Add Bash (shell)

Using the "Available Components" list, search for "bash" and click the plus sign to add it into the image.

image:labImages/addBash.png[]

Now notice that there are two tabs - *Selected Components* and *Dependencies*

image:labImages/selectedComponentsDependencies.png[]

Take a look through the dependencies that were added. 

image:labImages/baseDependencies.png[]

Now if you look at bash again, and go into it's details view, you can see;

* A package description.
* The ability to switch package versions, or restrict to a major/minor version.
* The dependencies, on which bash depends on (that were automatically added). 

image:labImages/bashDetails.png[]

=== Commit changes 

Changes to images are staged as a set of actions. They must be commited to take effect (or discarded). Review the changes by clicking the link next to the blue button, then Commit.

image:labImages/commitChanges.png[]

image:labImages/commitChanges2.png[]

Once commited, click the "Create image" button to open the "Create image" dialog; 

image:labImages/createImage.png[]

Make sure you select "Azure Disk Image" from the dropdown. Once the creation has started you should get a notification.

=== Monitoring image progress

The web interface shows images that have been built, and a basic view of images that are pending/in-progress. See below for an example.

image:labImages/imagesPending.png[]

To view more detail, we can look at the build logs.

Open up a terminal again (using the Cockpit web interface terminal, or your SSH session) and go look at the logs;
    
    root@workstation-GUID: cd /var/log/lorax-composer

In this directory you'll see a few log files. As the image is building, let's take a look at the `composer.log` files;

    root@workstation-GUID: tail -f composer.log

You should see a "kickstart", must like this;

    2019-04-29 07:54:29,353 INFO pylorax: disk_img = /var/lib/lorax/composer/results/544b4a3e-4fe1-49e3-b82f-0fc651985796/compose/disk.vhd
    2019-04-29 07:54:29,353 INFO pylorax: Using disk size of 4460MiB
    2019-04-29 07:54:29,355 INFO pylorax: Running anaconda.
    2019-04-29 07:54:33,646 INFO pylorax: Starting installer, one moment...
    2019-04-29 07:54:33,646 INFO pylorax: terminal size detection failed, using default width
    2019-04-29 07:54:33,647 INFO pylorax: anaconda 29.19.0.39-1.el8 for Red Hat Enterprise Linux 8.0 (pre-release) started.
    2019-04-29 07:54:33,647 INFO pylorax: 07:54:33 Not asking for VNC because of an automated install
    2019-04-29 07:54:33,647 INFO pylorax: 07:54:33 Not asking for VNC because we don't have Xvnc
    2019-04-29 07:54:43,679 INFO pylorax: Starting automated install........
    2019-04-29 07:54:43,679 INFO pylorax: Generating updated storage configuration
    2019-04-29 07:54:43,679 INFO pylorax: Checking storage configuration...

Let's review what is happening here

    FIXME diagram showing kickstart

The install should take 5-7 minutes, you should sit and watch the install until it completes.

When you see logs like this, **you are about half way through**;

    2019-04-29 07:57:02,456 INFO pylorax: Verifying dracut-squash.x86_64 (368/416)
    2019-04-29 07:57:02,456 INFO pylorax: Verifying python3-gobject-base.x86_64 (369/416)
    2019-04-29 07:57:02,456 INFO pylorax: Verifying polkit-pkla-compat.x86_64 (370/416)
    2019-04-29 07:57:02,456 INFO pylorax: Verifying libusbx.x86_64 (371/416)
    2019-04-29 07:57:02,456 INFO pylorax: Verifying ncurses-libs.x86_64 (372/416)
    2019-04-29 07:57:02,456 INFO pylorax: Verifying gobject-introspection.x86_64 (373/416)

When you see a log message like this, **the process is complete**; 

    2019-04-29 07:58:22,308 INFO pylorax: Finished building /var/lib/lorax/composer/queue/run/544b4a3e-4fe1-49e3-b82f-0fc651985796, results are in /var/lib/lorax/composer/results/544b4a3e-4fe1-49e3-b82f-0fc651985796

You can stop scrolling the logs by pressing **Ctrl + C**.

As the log message suggests, the results are in /var/lib/lorax/composer/ . A random ID number is generated for each image build. Lets go and find your image.

    root@workstation-GUID: cd /var/lib/lorax/composer/results/

The directory that contains your image is based on a random ID, you should just see one sub directory in the "results" directory though.

If all is well, run a `ll` or `ls -l`, to find the results;

    root@workstation-GUID: ll
    total 1388056
    -rw-r--r--. 1 root weldr        122 Apr 29 07:54 blueprint.toml
    -rw-r--r--. 1 root weldr         40 Apr 29 07:54 COMMIT
    -rw-r--r--. 1 root weldr        938 Apr 29 07:54 config.toml
    -rw-r--r--. 1 root weldr      10448 Apr 29 07:54 deps.toml
    -rw-r--r--. 1 root weldr 4676649472 Apr 29 07:58 disk.vhd
    -rw-r--r--. 1 root weldr       5188 Apr 29 07:54 final-kickstart.ks
    -rw-r--r--. 1 root weldr        162 Apr 29 07:54 frozen.toml
    drwxr-xr-x. 3 root weldr         22 Apr 29 07:58 logs
    -rw-r--r--. 1 root weldr          9 Apr 29 07:58 STATUS
    -rw-r--r--. 1 root weldr         88 Apr 29 07:58 times.toml
    -rw-r--r--. 1 root weldr       1605 Apr 29 07:54 vhd.ks
    
Your built Azure RHEL disk image is called `disk.vhd`.

disk.vhd:: The Azure disk image that was built, and would be uploaded to Azure.
logs:: log files that were generated during the build/install. 
deps.toml:: The package dependencies of the image. 
vhd.ks:: The kickstart file defined specifically for the Azure VHD format.
final-kickstart.ks:: The generated kickstart file, including `vhd.ks`, and
packages.  

=== Inspect 

FIXME description of other files in this directory.

=== Inspect the kickstart file

The composer process generates a "kickstart" file for you to install -
**final-kickstart.ks**. 

FIXME more detail.

=== Review, and verifying the completed image

In this section, we have;

. Used the simple RHEL8 web interface to setup a simple image blueprint
. Submitted that blueprint to be built, which used Anaconda in the background
. Verified that the image built, which is the *disk.vhd* file.

== Customization with `composer-cli`

The web interface that we have used so far is great for getting started. There
is also a command line tool - `composer-cli`. Like any other standard tool, you
can type `composer-cli [TAB]` on the command line to complete commands too.

Let's list now use the cli to find the blueprint we just created;

    root@workstation-GUID: composer-cli blueprints list
    example-atlas
    example-development
    example-http-server
    goldimage

    root@workstation-GUID: composer-cli blueprints show goldimage
    name = "goldimage"
    description = ""
    version = "0.0.2"
    modules = []
    groups = []

    [[packages]]
    name = "bash"
    version = "*"

When you looked at the logs for the image as it was being built, you should
have seen several RPM packages being installed. They were installed from a
"package source", which is the lab repository that we configured earlier. Take
a look; 

    root@workstation-GUID: composer-cli sources list
    rhel-8-for-x86_64-appstream-htb-rpms
    rhel-8-for-x86_64-baseos-htb-rpms

Now you can see how blueprints have changed over time, using the "changes"
subcommand;

----
root@workstation-GUID: composer-cli blueprints changes goldimage
goldimage
    2019-04-10T20:53:33Z  74b05113795ca6e40596ecf574957b1b325957d8
    Recipe goldimage, version 0.0.2 saved.

    2019-04-10T20:51:42Z  c3ea174ba967a63fa30e82a07f39822187092fdd
    Recipe goldimage, version 0.0.1 saved.
----

=== Add an additional package

Lets add a package to the gold image via the command line. We first "check out"
a definition of the gold image as a TOML file (a markup language, like .ini and
 .yaml are). Edit that file, and then push it back to the server.

    root@workstation-GUID: composer-cli blueprints save goldimage

This should create a file called "goldimage.toml". Edit this file with your
favorite editor to add the `httpd` package to this image. When finished, it
should look similar to this;

----
name = "goldimage"
description = ""
version = "0.0.3"
modules = []
groups = []

[[packages]]
name = "bash"
version = "*"

[[packages]]
name = "httpd"
version = "*"
----

[IMPORTANT]
It is important to update the version number in the blueprint. For example, the
version number was 0.0.2, you need to update it to 0.0.3 before pushing it back
to the server.

Now lets save the file back to the server;

    root@workstation-GUID: composer-cli blueprints push goldimage.toml

If this completed successfully, there is normally no output. 

Let's check that the file was actually saved, if a new version was added since
you last ran the "changes" command;

----
root@workstation-GUID: composer-cli blueprints changes goldimage
goldimage

    2019-04-29T15:55:32Z  db8f555fce2b7876365640ba44cc50ff2adc05c3
    Recipe goldimage, version 0.0.3 saved.

    2019-04-10T20:53:33Z  74b05113795ca6e40596ecf574957b1b325957d8
    Recipe goldimage, version 0.0.2 saved.

    2019-04-10T20:51:42Z  c3ea174ba967a63fa30e82a07f39822187092fdd
    Recipe goldimage, version 0.0.1 saved.
----

It would be necessary now to rebuild your .vhd Azure disk image for the httpd
package to be included. 

=== Root SSH key

==== Generate SSH key

FIXME 

=== Adding user accounts

You need to be able to login to the image once you provision it, and for that
you will need to create additional user accounts.

Using the steps from before, **save** a new version of the blueprint, and
edit it as follows to add a admin user;

    [[customizations.user]]
    name = "admin"
    description = "Administrator account"
    password = "$6$CHO2$3rN8eviE2t50lmVyBYihTgVRHcaecmeCk31L..."
    key = "PUBLIC SSH KEY"
    home = "/srv/widget/"
    shell = "/usr/bin/bash"
    groups = ["widget", "users", "wheel"]
    uid = 1200
    gid = 1200

FIXME some of the fields above

=== Adding user groups

FIXME

////
=== Common issues
lorax-composer.socket is not enabled
Error message in the web console: 'An error occurred. not-found'
////



